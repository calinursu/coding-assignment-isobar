import { useState, useEffect } from 'react';
import shuffleArray from '../utils/shuffleArray';

const useData = () => {
  const [stories, setStories] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);

    const getStories = async () => {
        const url = 'https://hacker-news.firebaseio.com/v0/topstories.json';
        try {
            const response = await fetch(url);
            if (!response.ok) {
                setLoading(false);
                throw new Error(`${response.status} ${response.statusText}`);
            }

            let data = await response.json();
            data = shuffleArray(data).slice(0, 10);

            const storyItemPromises = data
                .map((storyId: number) => fetch(`https://hacker-news.firebaseio.com/v0/item/${storyId}.json`).then(response => response.json()));    
            const storyItemResults = await Promise.all(storyItemPromises);

            const userPromises = storyItemResults
                .map((item: any) => fetch(`https://hacker-news.firebaseio.com/v0/user/${item.by}.json`).then(response => response.json()));
            const userResults = await Promise.all(userPromises);

            const results = storyItemResults
                .map((story: any, i: number) => ({ ...story, karma: userResults[i].karma }))
                .sort((a, b) => a.score - b.score) as any;

            setStories(results);
            setLoading(false);
        } catch (error) {
            setLoading(false);
            throw new Error( `${error}`);
        }
    }
    getStories();
  }, []);

  return { loading, stories };
};

export default useData;

