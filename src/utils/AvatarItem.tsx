import * as React from 'react';
import { ReactComponent as Avatar } from '../images/avatar.svg';

const AvatarItem = () => <Avatar title="Avatar" className="avatar" />;

export default AvatarItem;