const shuffleArray = (arr: any) => arr.sort(() => 0.5 - Math.random());

export default shuffleArray;