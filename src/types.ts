export interface Story {
    id: number;
    by: string;
    title: string;
    url: string;
    score: number;
    karma: number;
    time: number;
}