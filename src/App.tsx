import * as React from 'react';
import ListItem from './components/ListItem';
import useData from './hooks/useData';
import './styles.scss';
import { Story } from './types';

const App = () => {
  const { loading, stories } = useData();

  return (
    <div>
      {loading ? (
        <h4 className="loading">Loading...</h4>
      ) : (
        <div className="story-list">
          {stories.map((story: Story) => (
            <ListItem key={story.id} {...story} />
          ))}
        </div>
      )}
    </div>
  );
}

export default App;
