import * as React from 'react';
import { FC } from 'react';

interface Props {
  url: string;
  title: string;
}

const Link: FC<Props> = ({ url, title }) => (
  <a href={url} target="_blank" rel="noreferrer">
    {title}
  </a>
);

export default Link;