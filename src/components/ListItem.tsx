import * as React from 'react';
import { FC } from 'react';
import AvatarItem from '../utils/AvatarItem';
import Link from './Link';
import { Story } from '../types';

const ListItem: FC<Story> = ({ by, title, url, score, karma, time }) => (
    <div className="story">
      <span className="arrow"></span>
      <div className="story-text-container">
        <h3 className="story-title"><Link url={url} title={title} /></h3>
        <div className="story-info">{score} points by <AvatarItem /> {by} {time} ms ago | karma {karma}</div>
      </div>
    </div>
  );


export default ListItem;